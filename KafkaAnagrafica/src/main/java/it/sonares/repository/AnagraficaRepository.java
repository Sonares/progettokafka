package it.sonares.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.sonares.entity.Anagrafica;

@Repository
public interface AnagraficaRepository extends JpaRepository<Anagrafica, Long> {

	public Optional<Anagrafica> findByNomeAndCognome(String nome, String cognome);

}
