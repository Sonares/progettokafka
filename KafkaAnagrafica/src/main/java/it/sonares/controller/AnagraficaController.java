package it.sonares.controller;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.sonares.entity.Anagrafica;
import it.sonares.service.AnagraficaService;

@RestController
@RequestMapping("/anag")
public class AnagraficaController {

	@Autowired
	private AnagraficaService service;

	@GetMapping("/")
	public ResponseEntity<List<Anagrafica>> findAll() {
		return new ResponseEntity<List<Anagrafica>>(service.findAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Anagrafica> findById(@PathVariable("id") @NotNull Long id) {
		return new ResponseEntity<Anagrafica>(service.findById(id), HttpStatus.OK);
	}

	@PostMapping("/")
	public ResponseEntity<Anagrafica> save(@RequestParam("nome") String nome, @RequestParam("cognome") String cognome) {
		return new ResponseEntity<Anagrafica>(service.save(nome, cognome), HttpStatus.CREATED);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> deleteById(@PathVariable("id") Long id) {
		return new ResponseEntity<Boolean>(service.deleteById(id), HttpStatus.OK);
	}

}
