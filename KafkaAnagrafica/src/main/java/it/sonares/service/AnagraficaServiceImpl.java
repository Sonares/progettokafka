package it.sonares.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import it.sonares.config.Topics;
import it.sonares.entity.Anagrafica;
import it.sonares.repository.AnagraficaRepository;

@Service
public class AnagraficaServiceImpl implements AnagraficaService {

	@Autowired
	private KafkaTemplate<String, Anagrafica> template;

	@Autowired
	private AnagraficaRepository anagraficaRepository;

	@Override
	public List<Anagrafica> findAll() {
		return anagraficaRepository.findAll();
	}

	@Override
	public Anagrafica findById(Long id_anag) {
		return anagraficaRepository.findById(id_anag).orElse(null);
	}

	@Override
	public Anagrafica save(String nome, String cognome) {
		if (StringUtils.isEmpty(nome) || StringUtils.isEmpty(cognome))
			return null;
		
		if (anagraficaRepository.findByNomeAndCognome(nome, cognome).isPresent())
			return null;

		Anagrafica persistedAnag = anagraficaRepository.saveAndFlush(new Anagrafica(nome, cognome));

		/* Invio notifica sul topic "topic-anagrafica" */
		Message<Anagrafica> message = MessageBuilder
										.withPayload(persistedAnag)
										.setHeader(KafkaHeaders.TOPIC, Topics.ANAGRAFICA_CONTO)
										.build();
		template.send(message);

		return persistedAnag;
	}

	@Override
	public boolean deleteById(Long id_anag) {
		if (id_anag == null || id_anag.longValue() == 0L)
			return false;

		anagraficaRepository.deleteById(id_anag);

		return !anagraficaRepository.existsById(id_anag);
	}

}
