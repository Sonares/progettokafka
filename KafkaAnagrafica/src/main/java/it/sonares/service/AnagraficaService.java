package it.sonares.service;

import java.util.List;

import it.sonares.entity.Anagrafica;

public interface AnagraficaService {

	public List<Anagrafica> findAll();

	public Anagrafica findById(Long id_anag);

	public Anagrafica save(String nome, String cognome);

	public boolean deleteById(Long id_anag);

}
