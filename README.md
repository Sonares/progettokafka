# ProgettoKafka


## Progetto per la dimostrazione del funzionamento di Kafka per microservizi Java.  
In pratica, all'attivo, si avrà un sistema multi broker (nello specifico saranno attive in parallelo 3 istanze di kafka server), al quale saranno collegati 3 microservizi (Anagrafica, Conto, Movimenti) con i corrispettivi 3 DB SQLServer.  
Il numero di broker e/o microservizi non è vincolante, possono esserci N broker in parallelo su cui far girare M microservizi ovviamente.  
---

## Il primo giro funzionale è il seguente:  
1. Il servizio di Anagrafica (localhost:9001) permette di creare un nuovo cliente.  
2. Appena creato, il servizio di Anagrafica manda un messaggio su un topic specifico di Kafka (chiamiamolo Topic A) per avvertire eventuali altri microservizi (quindi fa da producer).  
3. Il servizio di Conto (localhost:9002) è in ascolto su quel topic (quindi fa da consumer di Topic A) e, appena riceve un messaggio, scrive sul suo DB un rigo rappresentante il conto corrispondente al nuovo cliente.  
4. Finita la scrittura, manda un messaggio su un altro topic di Kafka (quindi fa anche da producer, su un Topic B).  
5. Il servizio di Movimenti (localhost:9003) è in ascolto su quest'ultimo topic (Topic B) e crea il primo movimento di apertura conto sul proprio DB.  
---

## La logica di funzionamento post creazione, è la seguente:
1. Il servizio di Movimento (localhost:9003) viene richiamato per, appunto, effettuare un movimento. Quindi, quando riceve la chiamata, scrive il movimento sul proprio DB.  
2. Finita la scrittura, invia un messaggio su un topic di Kafka (quindi fa da producer su un Topic C).  
3. Il servizio di Conto (localhost:9002) è in ascolto sul topic di cui sopra (quindi fa da consumer su Topic C) ed aggiorna il bilancio sul proprio DB.  
---

# Istruzioni per l'uso  

### NB: Se il prompt dei comandi vi restituisce un errore del tipo ` Linea in ingresso troppo lunga. Sintassi del comando errata `, spostare la cartella (dell'intero progetto oppure anche solo di kafka va bene) in un path "più corto", non so perché Windows dà questi problemi di tanto in tanto.

## Avvio Kafka
1. Bisogna avviare prima Zookeeper (tool di Apache che permette la gestione delle partizioni, gli offset per l'indicizzazione dei messaggi da inviare ai consumer, ecc).  
2. Andare al path "...\progettokafka\kafka\bin\windows", avviare un prompt dei comandi (o powershell) ed inserire:  
` .\zookeeper-server-start.bat ..\..\config\zookeeper.properties `  
3. Una volta avviato Zookeeper, si possono avviare i 3 server Kafka.
4. Aprire altri 3 terminali ed eseguire i seguenti comandi:  
` .\kafka-server-start.bat ..\..\config\server.properties `  
` .\kafka-server-start.bat ..\..\config\server-1.properties `   
` .\kafka-server-start.bat ..\..\config\server-2.properties `  
5. A questo punto dovreste avere attivo il Cluster MultiBroker di Kafka (se volete saperne di più, potete aprire i file .properties per le configurazioni).  


## Creazione DB
1. Create i 3 DB. Passaggio ovvio, ma magari vi sfugge all'inizio. I nomi che ho dato io sono "Anagrafica", "Conto" e "Movimenti".


## Avvio dei microservizi
1. Modificare l'application.yml con le proprie configurazioni di SQL Server (del primo profilo, quello demoninato "dev", il secondo riguarda Docker).  
2. Build di Maven di tutti e 3 i microservizi ed esecuzione. Nessun ordine particolare, sono isolati su questo punto di vista.  
    2.1 Modificate anche il valore della proprietà "spring.jpa.hibernate.ddl-auto", io l'ho lasciato in update così dovete solo creare i 3 DB, alle tabelle ci pensa lui. Se volete fare da voi, mettete pure "none".  
	
