package it.sonares.entity;

public class Anagrafica {

	private Long id_anag;
	private String nome;
	private String cognome;

	public Anagrafica() {
	}

	public Anagrafica(Long id_anag, String nome, String cognome) {
		this.id_anag = id_anag;
		this.nome = nome;
		this.cognome = cognome;
	}

	public Long getId_anag() {
		return id_anag;
	}

	public void setId_anag(Long id_anag) {
		this.id_anag = id_anag;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	@Override
	public String toString() {
		return "Anagrafica [id_anag=" + id_anag + ", nome=" + nome + ", cognome=" + cognome + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cognome == null) ? 0 : cognome.hashCode());
		result = prime * result + ((id_anag == null) ? 0 : id_anag.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Anagrafica other = (Anagrafica) obj;
		if (cognome == null) {
			if (other.cognome != null)
				return false;
		} else if (!cognome.equals(other.cognome))
			return false;
		if (id_anag == null) {
			if (other.id_anag != null)
				return false;
		} else if (!id_anag.equals(other.id_anag))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

}
