package it.sonares.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "movimento")
public class Movimento {

	@Id
	@Column(name = "ID_MOVIMENTO", length = 100, nullable = false)
	private String idMovimento;

	@Column(name = "ID_ANAG", nullable = false)
	private Long idAnag;

	@Column(name = "NUM_CONTO", length = 100, nullable = false)
	private String numConto;

	@Column(name = "DATA_MOVIMENTO", nullable = false)
	private Date dataMovimento;

	@Column(name = "IMPORTO", nullable = false)
	private Float importo;

	@Column(name = "CAUSALE", nullable = false)
	private String causale;

	public Movimento() {
	}

	public Movimento(String idMovimento, Long idAnag, String numConto, Date dataMovimento, Float importo,
			String causale) {
		this.idMovimento = idMovimento;
		this.idAnag = idAnag;
		this.numConto = numConto;
		this.dataMovimento = dataMovimento;
		this.importo = importo;
		this.causale = causale;
	}

	public String getIdMovimento() {
		return idMovimento;
	}

	public void setIdMovimento(String idMovimento) {
		this.idMovimento = idMovimento;
	}

	public Long getIdAnag() {
		return idAnag;
	}

	public void setIdAnag(Long idAnag) {
		this.idAnag = idAnag;
	}

	public String getNumConto() {
		return numConto;
	}

	public void setNumConto(String numConto) {
		this.numConto = numConto;
	}

	public Date getDataMovimento() {
		return dataMovimento;
	}

	public void setDataMovimento(Date dataMovimento) {
		this.dataMovimento = dataMovimento;
	}

	public Float getImporto() {
		return importo;
	}

	public void setImporto(Float importo) {
		this.importo = importo;
	}

	public String getCausale() {
		return causale;
	}

	public void setCausale(String causale) {
		this.causale = causale;
	}

	@Override
	public String toString() {
		return "Movimento [idMovimento=" + idMovimento + ", idAnag=" + idAnag + ", numConto=" + numConto
				+ ", dataMovimento=" + dataMovimento + ", importo=" + importo + ", causale=" + causale + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((causale == null) ? 0 : causale.hashCode());
		result = prime * result + ((dataMovimento == null) ? 0 : dataMovimento.hashCode());
		result = prime * result + ((idAnag == null) ? 0 : idAnag.hashCode());
		result = prime * result + ((idMovimento == null) ? 0 : idMovimento.hashCode());
		result = prime * result + ((importo == null) ? 0 : importo.hashCode());
		result = prime * result + ((numConto == null) ? 0 : numConto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movimento other = (Movimento) obj;
		if (causale == null) {
			if (other.causale != null)
				return false;
		} else if (!causale.equals(other.causale))
			return false;
		if (dataMovimento == null) {
			if (other.dataMovimento != null)
				return false;
		} else if (!dataMovimento.equals(other.dataMovimento))
			return false;
		if (idAnag == null) {
			if (other.idAnag != null)
				return false;
		} else if (!idAnag.equals(other.idAnag))
			return false;
		if (idMovimento == null) {
			if (other.idMovimento != null)
				return false;
		} else if (!idMovimento.equals(other.idMovimento))
			return false;
		if (importo == null) {
			if (other.importo != null)
				return false;
		} else if (!importo.equals(other.importo))
			return false;
		if (numConto == null) {
			if (other.numConto != null)
				return false;
		} else if (!numConto.equals(other.numConto))
			return false;
		return true;
	}

}
