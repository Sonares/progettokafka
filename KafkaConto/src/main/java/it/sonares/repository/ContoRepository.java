package it.sonares.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.sonares.entity.Conto;

@Repository
public interface ContoRepository extends JpaRepository<Conto, Long> {

	public Optional<Conto> findByNumConto(String numConto);

}
