package it.sonares.controller;

import java.util.List;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.sonares.entity.Conto;
import it.sonares.service.ContoService;

@RestController
@RequestMapping("/conto")
public class ContoController {

	@Autowired
	private ContoService service;

	@GetMapping("/")
	public ResponseEntity<List<Conto>> findAll() {
		return new ResponseEntity<List<Conto>>(service.findAll(), HttpStatus.OK);
	}

	@GetMapping("/{numConto}")
	public ResponseEntity<Conto> findByNumConto(@PathVariable("numConto") @NotBlank String numConto) {
		return new ResponseEntity<Conto>(service.findByNumConto(numConto), HttpStatus.OK);
	}

}
