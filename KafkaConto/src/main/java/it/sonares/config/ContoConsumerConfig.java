package it.sonares.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import it.sonares.entity.Anagrafica;
import it.sonares.entity.Movimento;

@EnableKafka
@Configuration
public class ContoConsumerConfig {

	@Value("${spring.kafka.bootstrap-servers}")
	private String bootstrapServers;

	@Bean
	public Map<String, Object> consumerConfigs() {
		Map<String, Object> props = new HashMap<>();

		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "group_id_1");

		return props;
	}

	@Bean
	public ConsumerFactory<String, Movimento> movimentoConsumerFactory() {
		return new DefaultKafkaConsumerFactory<>(consumerConfigs(), new StringDeserializer(),
				new JsonDeserializer<>(Movimento.class));
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, Movimento> movmentoKafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, Movimento> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(movimentoConsumerFactory());
		return factory;
	}

	@Bean
	public ConsumerFactory<String, Anagrafica> anagraficaConsumerFactory() {
		return new DefaultKafkaConsumerFactory<>(consumerConfigs(), new StringDeserializer(),
				new JsonDeserializer<>(Anagrafica.class));
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, Anagrafica> anagraficaKafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, Anagrafica> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(anagraficaConsumerFactory());
		return factory;
	}

}
