package it.sonares.config;

public class Topics {

	public static final String ANAGRAFICA_CONTO = "anagrafica-conto";

	public static final String CONTO_MOVIMENTI = "conto-movimenti";

	public static final String MOVIMENTI_CONTO = "movimenti-conto";
	
}
