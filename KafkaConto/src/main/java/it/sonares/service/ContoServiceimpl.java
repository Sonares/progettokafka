package it.sonares.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import it.sonares.config.Topics;
import it.sonares.entity.Anagrafica;
import it.sonares.entity.Conto;
import it.sonares.entity.Movimento;
import it.sonares.repository.ContoRepository;
import net.bytebuddy.utility.RandomString;

@Service
public class ContoServiceimpl implements ContoService {

	private final Logger log = LoggerFactory.getLogger(ContoServiceimpl.class);

	@Autowired
	private ContoRepository contoRepository;

	@Autowired
	private KafkaTemplate<String, Conto> template;

	@Override
	public List<Conto> findAll() {
		return contoRepository.findAll();
	}

	@Override
	public Conto findByNumConto(String numConto) {
		return contoRepository.findByNumConto(numConto).orElse(null);
	}

	@Override
	public Conto findById(Long id) {
		return contoRepository.findById(id).orElse(null);
	}

	@KafkaListener(topics = Topics.ANAGRAFICA_CONTO, containerFactory = "anagraficaKafkaListenerContainerFactory")
	@Override
	public void saveFromAnagrafica(@Payload Anagrafica anagrafica) throws ParseException {
		Conto newConto = new Conto(anagrafica.getId_anag(), RandomString.make(100), new Date(),
				new SimpleDateFormat("dd-MM-yyyy").parse("31-12-2999"), 0F);

		Conto persistedConto = contoRepository.saveAndFlush(newConto);

		Message<Conto> message = MessageBuilder.withPayload(persistedConto)
				.setHeader(KafkaHeaders.TOPIC, Topics.CONTO_MOVIMENTI).build();

		template.send(message);
	}

	@KafkaListener(topics = Topics.MOVIMENTI_CONTO, containerFactory = "movmentoKafkaListenerContainerFactory")
	@Override
	public void saveFromMovimento(@Payload Movimento movimento) {
		Conto originalConto = contoRepository.findByNumConto(movimento.getNumConto()).orElse(null);

		if (originalConto == null) {
			log.error("Nessun conto trovato per il movimento -> " + movimento.getIdMovimento());
			return;
		}

		originalConto.setBilancio(originalConto.getBilancio() + movimento.getImporto());
		Float bilancio = contoRepository.saveAndFlush(originalConto).getBilancio();

		log.info("Conto aggiornato. Bilancio attuale: " + bilancio);
	}

}
