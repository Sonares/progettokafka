package it.sonares.service;

import java.text.ParseException;
import java.util.List;

import it.sonares.entity.Anagrafica;
import it.sonares.entity.Conto;
import it.sonares.entity.Movimento;

public interface ContoService {

	public List<Conto> findAll();

	public Conto findByNumConto(String numConto);

	public Conto findById(Long id);

	public void saveFromAnagrafica(Anagrafica anagrafica) throws ParseException;

	public void saveFromMovimento(Movimento movimento);

}
