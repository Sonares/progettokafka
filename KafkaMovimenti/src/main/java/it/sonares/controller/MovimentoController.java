package it.sonares.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.sonares.entity.Movimento;
import it.sonares.service.MovimentoService;

@RestController
@RequestMapping("/mov")
public class MovimentoController {

	@Autowired
	private MovimentoService service;

	@GetMapping("/id/{idAnag}")
	public ResponseEntity<List<Movimento>> findAllByIdAnag(@PathVariable("idAnag") Long idAnag) {
		return new ResponseEntity<List<Movimento>>(service.findAllByIdAnag(idAnag), HttpStatus.OK);
	}

	@GetMapping("/conto/{numConto}")
	public ResponseEntity<List<Movimento>> findAllByNumConto(@PathVariable("numConto") String numConto) {
		return new ResponseEntity<List<Movimento>>(service.findAllByNumConto(numConto), HttpStatus.OK);
	}

	@PostMapping("/{numConto}")
	public ResponseEntity<Movimento> pay(@PathVariable("numConto") String numConto,
										@RequestParam("importo") Float importo, 
										@RequestParam("causale") String causale) {
		return new ResponseEntity<Movimento>(service.pay(numConto, importo, causale), HttpStatus.OK);
	}
}
