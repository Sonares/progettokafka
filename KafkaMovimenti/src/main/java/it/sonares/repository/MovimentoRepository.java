package it.sonares.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.sonares.entity.Movimento;

@Repository
public interface MovimentoRepository extends JpaRepository<Movimento, String> {

	public List<Movimento> findByNumConto(String numConto);

	public List<Movimento> findByNumConto(String numConto, Sort sort);

	public List<Movimento> findByIdAnag(Long idAnag);

	public List<Movimento> findByIdAnag(Long idAnag, Sort sort);

}
