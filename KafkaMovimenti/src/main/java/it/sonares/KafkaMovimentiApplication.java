package it.sonares;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaMovimentiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaMovimentiApplication.class, args);
	}

}
