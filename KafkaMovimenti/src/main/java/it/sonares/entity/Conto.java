package it.sonares.entity;

import java.util.Date;

public class Conto {

	private Long id_anag;
	private String numConto;
	private Date dataInizio;
	private Date dataFine;
	private Float bilancio;

	public Conto() {
	}

	public Conto(Long id_anag, String numConto, Date dataInizio, Date dataFine, Float bilancio) {
		this.id_anag = id_anag;
		this.numConto = numConto;
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
		this.bilancio = bilancio;
	}

	public Long getId_anag() {
		return id_anag;
	}

	public void setId_anag(Long id_anag) {
		this.id_anag = id_anag;
	}

	public String getNumConto() {
		return numConto;
	}

	public void setNumConto(String numConto) {
		this.numConto = numConto;
	}

	public Date getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	public Date getDataFine() {
		return dataFine;
	}

	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}

	public Float getBilancio() {
		return bilancio;
	}

	public void setBilancio(Float bilancio) {
		this.bilancio = bilancio;
	}

	@Override
	public String toString() {
		return "Conto [id_anag=" + id_anag + ", numConto=" + numConto + ", dataInizio=" + dataInizio + ", dataFine="
				+ dataFine + ", bilancio=" + bilancio + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bilancio == null) ? 0 : bilancio.hashCode());
		result = prime * result + ((dataFine == null) ? 0 : dataFine.hashCode());
		result = prime * result + ((dataInizio == null) ? 0 : dataInizio.hashCode());
		result = prime * result + ((id_anag == null) ? 0 : id_anag.hashCode());
		result = prime * result + ((numConto == null) ? 0 : numConto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conto other = (Conto) obj;
		if (bilancio == null) {
			if (other.bilancio != null)
				return false;
		} else if (!bilancio.equals(other.bilancio))
			return false;
		if (dataFine == null) {
			if (other.dataFine != null)
				return false;
		} else if (!dataFine.equals(other.dataFine))
			return false;
		if (dataInizio == null) {
			if (other.dataInizio != null)
				return false;
		} else if (!dataInizio.equals(other.dataInizio))
			return false;
		if (id_anag == null) {
			if (other.id_anag != null)
				return false;
		} else if (!id_anag.equals(other.id_anag))
			return false;
		if (numConto == null) {
			if (other.numConto != null)
				return false;
		} else if (!numConto.equals(other.numConto))
			return false;
		return true;
	}

}
