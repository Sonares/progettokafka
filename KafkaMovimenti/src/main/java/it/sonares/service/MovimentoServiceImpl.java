package it.sonares.service;

import java.util.Date;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import it.sonares.config.Topics;
import it.sonares.entity.Conto;
import it.sonares.entity.Movimento;
import it.sonares.repository.MovimentoRepository;
import net.bytebuddy.utility.RandomString;

@Service
public class MovimentoServiceImpl implements MovimentoService {

	@Autowired
	private MovimentoRepository movimentoRepository;

	@Autowired
	private KafkaTemplate<String, Movimento> template;

	@Override
	public List<Movimento> findAll() {
		return movimentoRepository.findAll();
	}

	@Override
	public List<Movimento> findAllByNumConto(String numConto) {
		return movimentoRepository.findByNumConto(numConto, Sort.by("dataMovimento").descending());
	}

	@Override
	public List<Movimento> findAllByIdAnag(Long idAnag) {
		return movimentoRepository.findByIdAnag(idAnag, Sort.by("dataMovimento").descending());
	}

	@Override
	public Movimento findById(String idMovimento) {
		return movimentoRepository.findById(idMovimento).orElse(null);
	}

	@KafkaListener(topics = Topics.CONTO_MOVIMENTI)
	@Override
	public void saveFromConto(@Payload Conto conto) {
		Movimento newMovimento = new Movimento(RandomString.make(100), conto.getId_anag(), conto.getNumConto(),
				new Date(), 0F, "Apertura conto");

		if (movimentoRepository.saveAndFlush(newMovimento) != null)
			LoggerFactory.getLogger(MovimentoServiceImpl.class)
					.info("Creato movimento da topic Conto: " + newMovimento.getNumConto());
	}

	@Override
	public Movimento pay(String numConto, Float importo, String causale) {
		Long idAnag = movimentoRepository.findByNumConto(numConto).stream().findFirst().get().getIdAnag();

		Movimento mov = new Movimento(RandomString.make(100), idAnag, numConto, new Date(), importo, causale);
		Movimento persistedMovimento = movimentoRepository.saveAndFlush(mov);

		Message<Movimento> message = MessageBuilder.withPayload(persistedMovimento)
				.setHeader(KafkaHeaders.TOPIC, Topics.MOVIMENTI_CONTO).build();

		template.send(message);

		return persistedMovimento;
	}

}
