package it.sonares.service;

import java.util.List;

import it.sonares.entity.Conto;
import it.sonares.entity.Movimento;

public interface MovimentoService {

	public List<Movimento> findAll();

	public List<Movimento> findAllByNumConto(String numConto);

	public List<Movimento> findAllByIdAnag(Long idAnag);

	public Movimento findById(String idMovimento);

	public void saveFromConto(Conto conto);

	public Movimento pay(String numConto, Float importo, String causale);

}
